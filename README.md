# TalaMed Benchmark

This is the repository for comparing the performance and similarity between the new and legacy TalaMed search engine.

## Structure

* `evaluation/` - Contains the evaluation scripts and results.
* `logs/` - Contains the logs of the benchmark runs.
* `util/`- Contains the utility scripts for the benchmark.
* `benchmark.py` - The main script for running the benchmark.
* `LICENSE` - Contains a copy of the Apache 2.0 License.
* `phrases.csv` - Contains the phrases used as search terms in the benchmark.
* `words.csv` - Contains the single words used as search terms in the benchmark.
