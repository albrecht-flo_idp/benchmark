import argparse
import logging
import math
import time
import requests as req
import json
import random
import csv
import numpy as np
from threading import Thread

# To disable the warning due to disabled verification -> Only when running Benchmark
import urllib3

urllib3.disable_warnings()


def load_queries(filename: str) -> [str]:
    queries = []
    with open(filename, "r") as f:
        for line in f:
            line = line.strip()
            if not line.startswith("--") and line != "":
                queries.append(line.lower())
    queries = list(set(queries))  # Make unique
    return queries


def legacy_query(url, query, size, page=1):
    search_req = {
        "query": f"{query}",
        "facets": {
            "aktuell": {
                "type": "range",
                "ranges": [
                    {"from": -1, "name": "unwichtig"},
                    {"from": 0.00001, "name": "eher unwichtig"},
                    {"from": 0.75001, "name": "wichtig"},
                    {"from": 0.75001, "name": "sehr wichtig"},
                ],
            },
            "nutzerfreundlich": {
                "type": "range",
                "ranges": [
                    {"from": -1, "name": "unwichtig"},
                    {"from": 0.41671, "name": "eher unwichtig"},
                    {"from": 0.70831, "name": "wichtig"},
                    {"from": 0.70831, "name": "sehr wichtig"},
                ],
            },
            "vertrauenswuerdig": {
                "type": "range",
                "ranges": [
                    {"from": -1, "name": "unwichtig"},
                    {"from": 0.55701, "name": "eher unwichtig"},
                    {"from": 0.7210099999999999, "name": "wichtig"},
                    {"from": 0.7210099999999999, "name": "sehr wichtig"},
                ],
            },
            "verstaendlich": {
                "type": "range",
                "ranges": [
                    {"from": -1, "name": "unwichtig"},
                    {"from": 0.66811, "name": "eher unwichtig"},
                    {"from": 0.7895099999999999, "name": "wichtig"},
                    {"from": 0.7895099999999999, "name": "sehr wichtig"},
                ],
            },
        },
        "result_fields": {
            "content": {"raw": {}, "snippet": {"size": 100, "fallback": True}},
            "url": {"raw": {}, "snippet": {"size": 100, "fallback": True}},
            "aktuell": {"raw": {}, "snippet": {"size": 100, "fallback": True}},
            "nutzerfreundlich": {"raw": {}, "snippet": {"size": 100, "fallback": True}},
            "vertrauenswuerdig": {
                "raw": {},
                "snippet": {"size": 100, "fallback": True},
            },
            "verstaendlich": {"raw": {}, "snippet": {"size": 100, "fallback": True}},
            "title": {"raw": {}, "snippet": {"size": 100, "fallback": True}},
        },
        "page": {"size": size, "current": page},
    }
    search_req = json.dumps(search_req)
    headers = {
        "Content-Type": "application/json",
        "Authorization": "Bearer TOKEN",
    }
    res = req.post(url, headers=headers, data=search_req, verify=False)
    if not res.ok:
        raise RuntimeError(res)
    return res


def new_query(url, query, size, page=0, synonym_mode="legacy"):
    params = {"query": f"eq.{query}", "size": size, "page": page, "synonymMethod": synonym_mode}
    res = req.get(url, params=params, verify=False)
    if not res.ok:
        raise RuntimeError(res)
    return res


def test(url: str, queries: [str], test_func, result_list):
    logging.info("Test running")
    response_time = 0
    for i, query in enumerate(queries):
        res = test_func(url, query, 60)
        duration = res.elapsed.total_seconds()
        latency = res.headers['X-Backend-Latency']
        result = res.content
        result_list.append([i, query, duration, latency, result])
        response_time += duration
        logging.debug(f"Query {query}: duration={duration}s, latency={latency}s")
        if i % 10 == 0 or i == len(queries) - 1:
            logging.info(f"Test running {i}/{len(queries)} " +
                         f"avg response time {response_time / min(10, len(queries) - 1 % 10):.3f}s ...")
            response_time = 0


def multi_threaded_test(query_list, url, test_func, thread_count):
    batch_threads: [Thread] = [None for _ in range(thread_count)]
    batch_lists = [[] for _ in range(thread_count)]
    for j, batch in enumerate(np.array_split(query_list, thread_count)):
        batch_threads[j] = Thread(target=test, args=(url, batch, test_func, batch_lists[j]),
                                  name=f"test_thread_{j}")
        batch_threads[j].start()
    for thread in batch_threads:
        thread.join()
    return batch_lists


def main(legacy, N: int, dry=False, log_prefix="", synonym_mode=None):
    random.seed(42)
    word_queries: [str] = load_queries("words.csv")
    random.shuffle(word_queries)
    phrases_queries: [str] = load_queries("phrases.csv")
    random.shuffle(phrases_queries)

    LEGACY_URL = "https://api.virtlx164.aiim.med.tum.de/api/as/v1/engines/gap/search.json"
    NEW_URL = "https://new-app.virtlx164.aiim.med.tum.de/public/rest/v1/documents/search"

    logging.info(f"Using {len(word_queries)} word queries")
    logging.debug(f"    {word_queries}")
    logging.info(f"Using {len(phrases_queries)} phrase queries")
    logging.debug(f"    {phrases_queries}")

    test_func = legacy_query
    url = LEGACY_URL
    if not legacy:
        synonym_mode = "legacy" if synonym_mode is None else synonym_mode
        test_func = lambda url, query, size: new_query(url, query, size, 0, synonym_mode)
        url = NEW_URL

    logging.info(f"Testing against {url}")

    if dry:
        logging.warning(f"Dry run, exiting early...")
        return

    try:
        logging.info(f"Performing warmup with {math.ceil(N * 0.1)}/{N} iterations")

        logging.info(f"Performing warmup of word queries")
        for i in range(math.ceil(N * 0.1)):  # 10% iterations for warmup
            warmup_result = []
            test(url, word_queries, test_func, warmup_result)

        logging.info(f"Performing warmup of phrase queries")
        for i in range(math.ceil(N * 0.1)):  # 10% iterations for warmup
            warmup_result = []
            test(url, phrases_queries, test_func, warmup_result)

        # Shuffle again, to ensure different succession after warmup
        random.shuffle(word_queries)
        random.shuffle(phrases_queries)

        log_file_name = f"{log_prefix}{'LEGACY' if legacy else 'NEW'}-raw_log{time.strftime('%Y-%m-%d_%H-%M')}.csv"
        with open(log_file_name, 'w') as csv_file:
            writer = csv.writer(csv_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
            header = ['id', 'type', 'query', 'duration', 'latency', 'result']
            writer.writerow(header)

            thread_count = 1
            test_results_count = 0

            for i in range(N):
                test_results = []
                logging.info(f"Benchmarking word queries, run {i}/{N}")
                batch_lists = multi_threaded_test(word_queries, url, test_func, thread_count)
                # Combine results
                for batch in batch_lists:
                    for item in batch:
                        test_results.append([test_results_count, 'word', *item[1:]])
                        test_results_count += 1

                for result in test_results:
                    writer.writerow(result)

            for i in range(N):
                test_results = []
                logging.info(f"Benchmarking phrase queries, run {i}/{N}")
                batch_lists = multi_threaded_test(phrases_queries, url, test_func, thread_count)
                # Combine results
                for batch in batch_lists:
                    for item in batch:
                        test_results.append([test_results_count, 'phrase', *item[1:]])
                        test_results_count += 1

                for result in test_results:
                    writer.writerow(result)

        logging.info(f"Done, written log to {log_file_name}")
    except RuntimeError as ex:
        logging.exception(ex)


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        prog="Benchmark SearchApp",
        description="""This script performs our performance benchmark against the old and new SearchAPP.
        \n\n
        Example usage: python benchmark.py""",
    )

    parser.add_argument(
        "-v", "--verbose", action="store_true", help="enable debug printing"
    )
    parser.add_argument(
        "--log-file", action="store_true", help="enable logging to file"
    )
    parser.add_argument(
        "--legacy", action="store_true", help="Use Legacy SearchApp instead of new SearchApp"
    )
    parser.add_argument(
        "--dry", action="store_true", help="Dry run, do not query the API"
    )
    parser.add_argument(
        "-N", "--iter", default=10, type=int, help="Number or iterations to perform"
    )
    parser.add_argument(
        "--log_prefix", default="", help="Prefix for benchmark log"
    )
    parser.add_argument(
        "--synonym_mode", help="Prefix for benchmark log"
    )
    args = parser.parse_args()

    logging_handlers = [
        logging.StreamHandler(),
    ]
    if args.log_file:
        logging_handlers.append(
            logging.FileHandler(f"benchmark{time.strftime('%Y-%m-%d_%H-%M-%S')}.log"),
        )

    logging.basicConfig(
        level=logging.DEBUG if args.verbose else logging.INFO,
        handlers=logging_handlers,
    )

    if args.legacy and args.synonym_mode:
        logging.error("Cannot use synonym mode with legacy SearchApp")
        exit(1)

    main(args.legacy, N=args.iter, dry=args.dry, log_prefix=args.log_prefix, synonym_mode=args.synonym_mode)
